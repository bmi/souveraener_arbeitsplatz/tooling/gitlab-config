<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# Sovereign Workplace shared Gitlab CI/CD configuration

This repository contains Gitlab CI/CD configuration used in several projects to fully automate common Continuous
Integration (CI) and Continuous Delivery (CD) tasks.

[TOC]

## Requirements

For most of the common pipelines, it is required to have
[Gitlab Runner Cache](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runnerscache-section)
fully configured.

All features are only tested with
[Gitlab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/) and
[Gitlab Package Registry](https://docs.gitlab.com/ee/user/packages/package_registry/)

Due to the usage of Kaniko, we fully comply with **un**privileged runners.

## Usage

### Common

#### `common.yml` - The base for all

`./ci/common/common.yml`

The common folder contains references which are used in all following technology specific files.

#### Linting

`./ci/common/lint.yml`

Useful for all projects is the common linting configuration which helps to create a unified look and compliance.

**Available linters:**
- [yamllint](https://github.com/adrienverge/yamllint)
- [reuse](https://github.com/fsfe/reuse-tool)
- [conventional-commits](https://www.conventionalcommits.org/en/v1.0.0/)

##### yamllint

Yamllint is used in all `.yaml` and `.yml` files with a hardened rule set.

It requires f.e. double-quoted string, a line-length limit of 120 chars and a new line at the end of a file.

##### reuse

Reuse helps to ensure explicit and technically readable license and copyright identifiers for all files.

##### conventional-commits

All commits should be aligned with conventional commits styleguide to have a common committing style and use the commits
in release automation.

By default, commits are evaluated on branched against this commit regular expression:
`^(chore|ci|docs|feat|fix)(\([a-zA-Z0-9_/\.\\-]+\))?:\ (\[[a-z/0-9\\-_]+#[0-9]+\]\ )?[A-Z0-9]{1}.+$`.

This results in the following human-readable commit syntax guide:

```text
<type>(<scope>): [path/to/issue#1] <short summary>
  │       │              │                │
  │       │              |                └─> Summary in present tense, sentence case, with no period at the end
  │       │              |
  │       │              └─> Issue reference (optional)
  │       │
  │       └─> Commit Scope: myapp, docs, ... (optional)
  │
  └─> Commit Type: chore, ci, docs, feat, fix
```

This setting can change the regex:

```yaml
conventional-commits-linter:
  variables:
    COMMIT_REGEX: <your regex>
```

### Container Images

While we create container images that you could also call just "images" we prefer to use the wording "container"
when it comes to file- and variable names in this context as the word "image(s)" is just too ambiguous.

#### Container common

`./ci/container/container-common.yml`

Common settings and images are defined in this file.

#### Container build

`./ci/container/container-build.yml`

This CI configuration builds, pushes to a Gitlab registry, scans, generates SBOMs and finally signs and tags container images if all scans succeeded.

1. stage `build`
   - The `container-build` job builds the container image using kaniko and uploads it to the registry tagged with `${CI_COMMIT_SHA}`.
2. stage `scan`
   - The `container-clamav` job downloads the container image using crane and scans it using clamav.
   - The SBOMs are scanned in the `container-sbom` job regarding CVEs and are displayed in merge requests and `Code Quality` tab of the related CI.
   - The `container-scanning` job scans the container image using trivy regarding different security issues.
3. stage `release`
   - The `container-sign` job signs the container image using cosign.
   - The `container-tag` job tags the container image using crane.
     In the `${CI_DEFAULT_BRANCH}` branch generally the tag `latest` is used while semantic release additionally uses the release version.
     In other branches without semantic release the container gets tagged with `${CI_COMMIT_REF_SLUG}`. With semantic release the release version postfixed with `-${CI_COMMIT_REF_SLUG}` is used as tag.

##### Extends

- `./ci/common/common.yml`
- `./ci/container/container-common.yml`

##### Folder structure
```text
<repository root>
└── Dockerfile
```

##### Technologies

- [clamav](https://www.clamav.net/)
- [crane](https://github.com/google/go-containerregistry/tree/main/cmd/crane)
- [cosign](https://github.com/sigstore/cosign)
- [kaniko](https://github.com/GoogleContainerTools/kaniko)
- [trivy](https://github.com/aquasecurity/trivy)

##### Variables

| Name                           | Default                                 | Description                                                                            |
|--------------------------------|-----------------------------------------|----------------------------------------------------------------------------------------|
| `ADDITIONAL_ARGS`              |                                         | Additional command line arguments for kaniko build command                             |
| `ADDITIONAL_TAGS`              |                                         | Additional container tags, separated by " " (only supported in `${CI_DEFAULT_BRANCH}`) |
| `BUILD_CONTEXT  `              | `${CI_PROJECT_DIR}`                     | Directory which will be used as build context                                          |
| `BUILD_DESTINATION`            | `${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}` | Image name / Destination (before scanning and tagging)                                 |
| `CI_COMMIT_REF_NAME`           | Predefined Variable from Gitlab         | The branch or tag name for which project is built                                      |
| `CI_COMMIT_REF_SLUG`           | Predefined Variable from Gitlab         | The branch name without special chars except "-"                                       |
| `CI_COMMIT_SHA`                | Predefined Variable from Gitlab         | The commit revision the project is built for                                           |
| `CI_DEFAULT_BRANCH`            | Predefined Variable from Gitlab         | The name of the project’s default branch                                               |
| `CI_DEPENDENCY_PROXY_PASSWORD` | Predefined Variable from Gitlab         | DockerHub Mirror Password                                                              |
| `CI_DEPENDENCY_PROXY_SERVER`   | Predefined Variable from Gitlab         | DockerHub Mirror server                                                                |
| `CI_DEPENDENCY_PROXY_USER`     | Predefined Variable from Gitlab         | DockerHub Mirror username                                                              |
| `CI_REGISTRY`                  | Predefined Variable from Gitlab         | Registry server                                                                        |
| `CI_REGISTRY_IMAGE`            | Predefined Variable from Gitlab         | Image name including registry                                                          |
| `CI_REGISTRY_PASSWORD`         | Predefined Variable from Gitlab         | Registry Password                                                                      |
| `CI_REGISTRY_USER`             | Predefined Variable from Gitlab         | Registry username                                                                      |
| `COSIGN_PRIVATE_KEY`           |                                         | Private key used to sign Images                                                        |
| `DESIRED_TAG`                  | `latest`                                | Desired tag for container, will be modified in non `${CI_DEFAULT_BRANCH}` branches     |
| `DOCKERFILE`                   | `${CI_PROJECT_DIR}/Dockerfile`          | Path to Dockerfile                                                                     |

##### Usage

Add following snippets to your `.gitlab-ci.yml` file.

**Example 1:**

```yaml
---
include:
  project: "bmi/opendesk/tooling/gitlab-config"
  ref: "main"
  file:
    - "ci/container/container-build.yml"
...
```

**Example 2 (with linting):**

```yaml
---
include:
  project: "bmi/opendesk/tooling/gitlab-config"
  ref: "main"
  file:
    - "ci/common/lint.yml"
    - "ci/container/container-build.yml"

stages:
  - ".pre"
  - "lint"
  - "build"
  - "scan"
  - "release"
  - ".post"
...
```

#### Container lint

`./ci/container/container-lint.yml`

This CI configuration lints Dockerfiles.

##### Extends

- `./ci/common/common.yml`
- `./ci/container/container-common.yml`

##### Folder structure
```text
<repository root>
└── Dockerfile
```

##### Technologies

- [hadolint](https://github.com/hadolint/hadolint)

##### Variables

| Name                       | Default                        | Description                |
|----------------------------|--------------------------------|----------------------------|
| `HADOLINT_DOCKERFILE_PATH` | `${CI_PROJECT_DIR}/Dockerfile` | Path to Dockerfile to lint |

##### Usage

Add following snippets to your `.gitlab-ci.yml` file.

**Example 1:**

```yaml
---
include:
  project: "bmi/opendesk/tooling/gitlab-config"
  ref: "main"
  file:
    - "ci/container/container-lint.yml"
...
```

### Helm

This section is relevant when it comes to developing and releasing Helm charts. The available automation supports you
to get structured documentation, linting and semantic releases.

#### Helm common

`./ci/helm/helm-common.yml`

Common settings and images are defined in this file.

#### Helm docs

`./ci/helm/helm-docs.yml`

Based on comments in charts `values.yaml` helm-docs will generate a good looking `README.md`. Helm-docs allows to use an
own template to generate the projects `README.md`. Thus, we place a `README.md.gotmpl` instead of the `README.md` in the
charts root folder. If you want to use environment variables, to f.e. template the project id used in the registry, you
can instead place `README.md.gotmpl.tpl` which will be parsed through envsubst.

`README.md.gotmpl.tpl` has a higher precedence than `README.md.gotmpl`.

Multiple charts placed in the `<repository root>/charts` folder are supported.

##### Extends

- `./ci/common/common.yml`
- `./ci/helm/helm-common.yml`

##### Folder structure
```text
<repository root>/
├── charts/
│   └── <chart>/
│       ├── files/
│       ├── templates/
│       ├── Chart.yaml
│       ├── LICENSE
│       ├── README.md.gotmpl
│       ├── README.md.gotmpl.tpl
│       └── values.yaml
├── LICENSES/
└── .gitlab-ci.yml
```

##### Technolgies

- [gettext](https://www.gnu.org/software/gettext/)
- [helm-docs](https://github.com/norwoodj/helm-docs)

##### Usage

Add following Snippets to your `.gitlab-ci.yml` file.

**Example:**

```yaml
---
include:
  project: "bmi/opendesk/tooling/gitlab-config"
  ref: "main"
  file:
    - "ci/helm/helm-docs.yml"
...
```

#### Helm lint

`./ci/helm/helm-lint.yml`

Static application security testing (SAST) for Helm Charts.

##### Extends

- `./ci/common/common.yml`
- `./ci/helm/helm-common.yml`

##### Folder structure
```text
<repository root>/
├── charts/
│   └── <chart>/
│       ├── files/
│       ├── templates/
│       ├── Chart.yaml
│       ├── LICENSE
│       └── values.yaml
├── LICENSES/
└── .gitlab-ci.yml
```

##### Technolgies

- [kube-linter](https://github.com/stackrox/kube-linter)

##### Usage

Add following Snippets to your `.gitlab-ci.yml` file.

**Example:**

```yaml
---
include:
  project: "bmi/opendesk/tooling/gitlab-config"
  ref: "main"
  file:
    - "ci/helm/helm-lint.yml"
...
```

#### Helm release

`./ci/helm/helm-release.yml`

This Gitlab CI/CD configuration unleashes the full release lifecycle for helm charts. It covers: linting, docs
generation, packaging, publishing and release creation (include Semantic Versioning).

##### Extends

- `./ci/common/common.yml`
- `./ci/helm/helm-common.yml`
- `./ci/helm/helm-docs.yml`
- `./ci/release-automation/semantic-release.yml`

##### Folder structure
```text
<repository root>/
├── charts/
│   └── <chart>/
│       ├── files/
│       ├── templates/
│       ├── Chart.yaml
│       ├── LICENSE
│       ├── README.md.gotmpl.tpl
│       └── values.yaml
├── LICENSES/
└── .gitlab-ci.yml
```

##### Technolgies

- [gettext](https://www.gnu.org/software/gettext/)
- [helm](https://helm.sh)
- [helm-docs](https://github.com/norwoodj/helm-docs)
- [kube-linter](https://github.com/stackrox/kube-linter)

##### Variables

| Name                    | Default                         | Description                             |
|-------------------------|---------------------------------|-----------------------------------------|
| `GPG_SIGNING_KEY`       |                                 | GPG private key to sign helm charts     |
| `GPG_SIGNING_KEY_EMAIL` |                                 | Mail address to identify gpg key        |
| `CI_API_V4_URL`         | Predefined Variable from Gitlab | Gitlab API v4 address                   |
| `CI_JOB_TOKEN`          | Predefined Variable from Gitlab | Temporary access token to publish chart |
| `CI_PROJECT_ID`         | Predefined Variable from Gitlab | Project ID to identity Package Registry |
| (`RELEASE_VERSION`)     |                                 | Generated during CI runtime             |

##### Usage

Add following Snippets to your `.gitlab-ci.yml` file.

**Example:**

```yaml
---
include:
  project: "bmi/opendesk/tooling/gitlab-config"
  ref: "main"
  file:
    - "ci/common/lint.yml"
    - "ci/helm/helm-lint.yml"
    - "ci/helm/helm-release.yml"

stages:
  - ".pre"
  - "lint"
  - "docs"
  - "package"
  - "publish"
  - ".post"
...
```

### Release automation

#### Semantic release

`./ci/release-automation/semantic-release.yml`

Based on a common commit syntax ([conventional commits](https://www.conventionalcommits.org/en/v1.0.0/#specification))
Semantic Versioning (SemVer) git tags or release versions are created. The next version can be used in pipeline
via `RELEASE_VERSION` environment variable.

##### Technolgies

- [semantic-release](https://github.com/semantic-release/semantic-release)

##### Usage

Add following Snippets to your `.gitlab-ci.yml` file.

**Example 1:**

```yaml
---
include:
  project: "bmi/opendesk/tooling/gitlab-config"
  ref: "main"
  file:
    - "ci/release-automation/semantic-release.yml"
...
```

Note: Gitlab (tested 16.2.2) does not allow only `.pre` and `.post` stage. Easiest fix is to add linting to your pipeline.

**Example 2 (with lining):**

```yaml
---
include:
  project: "bmi/opendesk/tooling/gitlab-config"
  ref: "main"
  file:
    - "ci/release-automation/semantic-release.yml"
    - "ci/common/lint.yml"
...
```

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright © 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
